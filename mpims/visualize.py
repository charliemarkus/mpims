import numpy as np
from scipy.signal import find_peaks
import os
import matplotlib.pyplot as plt
import mpims.mpims.data_handler as dh


def plot_bin(SE:dh.Single_E, vmax=10):
    plt.figure(SE.root_name)
    plt.title(SE.root_name)
    plt.imshow(SE.data['Binned'][::-1], aspect='auto', interpolation='none', vmax=vmax,
                extent=(SE.data['M edge'][0],SE.data['M edge'][-1],
                        SE.data['T edge'][0],SE.data['T edge'][-1]))
    
    cbar = plt.colorbar()
    cbar.set_label('Counts')
    plt.xlabel("Mass (m/z)")
    plt.ylabel('Kinetic time (s)')
    plt.show()

def plot_bin_raw(SE:dh.Single_E, vmax=10):
    fig = plt.figure(SE.root_name)
    plt.title(SE.root_name)
    plt.imshow(SE.data['Binned raw'][::-1], aspect='auto', interpolation='none', vmax=vmax,
                extent=(SE.data['X edge'][0],SE.data['X edge'][-1],
                        SE.data['T edge'][0],SE.data['T edge'][-1]))
    
    cbar = plt.colorbar()
    cbar.set_label('Counts')
    plt.xlabel("TOF time (ns)")
    plt.ylabel('Kinetic time (s)')
    plt.show()
    return fig

def plot_bin_range(SE:dh.Single_E, m_bin, t_bin, first_shot, num_shots, vmax=10):
    hist, tedge, medge=  SE.bin_shot_range(m_bin, t_bin, first_shot, num_shots)
    plt.title(f"{SE.root_name}, shots {first_shot}-{first_shot+num_shots}")
    plt.imshow(hist[::-1], aspect='auto', interpolation='none', vmax=vmax,
                extent=(medge[0],medge[-1],
                        tedge[0],tedge[-1]))
    plt.xlabel("Mass (m/z)")
    plt.ylabel('Kinetic time (s)')
    plt.show()
def plot_mass_spectrum(SE:dh.Single_E):

    pass