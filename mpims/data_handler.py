import numpy as np
import struct 
from scipy.signal import find_peaks
import os

def load_binary_data(name):
    fbin = open(name, 'rb')

    data = fbin.read()
    num_integers = len(data) // 4
    values = np.array(struct.unpack('<{}i'.format(num_integers), data), dtype='float')
    return values

def load_binary_data_range(name, start_index=0, end_index=10000000):
    start_byte = start_index * 4
    end_byte = end_index * 4

    with open(name, 'rb') as fbin:
        fbin.seek(start_byte)

        num_bytes = end_byte - start_byte
        data = fbin.read(num_bytes)

        num_integers = len(data) // 4

        values = np.array(struct.unpack('<{}i'.format(num_integers), data), dtype='float')

        return values


    

def save_binary_data(name, values:np.array):
    values_int = values.astype('int32')
    data = struct.pack('<{}i'.format(len(values_int)), *values_int)
    fout = open(name,'wb')
    fout.write(data)
    fout.close()

# Finds index nearest to x among values
def value_ind(values, x):
    return np.argmin(abs(values - x))

#For plotting histograms vs 1d data
def edge_to_mid(edges):
    e_array = np.vstack([edges[:-1], edges[1:]])
    return np.mean(e_array, axis=0)

#for removing isolated signals when trying to align shots
#x: data
#rng: how far to sum points
# threshold: If sum is below this mask point
def mask_noise(x:np.array, rng=1, threshold=2):
    x_out = x.copy()
    for i in range(len(x)):
        if np.sum(x[i-rng:i+rng]) < threshold:
            x_out[i] = 0
    return x_out

class Single_E:
    def __init__(self, directory, root_name, num=-1):
        self.dir = directory
        self.root_name = root_name
        self.num=num
        

        self.data = {}
        self.info = {}

        self.read_params()
        self.load_data()
        self.find_sweep_inds()
        self.set_time()


    def read_params(self):
        param_name = f"{self.root_name}.PAR"
        param_path = f"{self.dir}{param_name}"
        f = open(param_path, 'r')

        param_list = []
        for line in f:
            param_list.append(line.rstrip())
    
        name = 'Rep Rate (ms)'
        self.info[name] = float(param_list[param_list.index(name)+1])
        name = 'TOF Sweep (us)'
        self.info[name] = float(param_list[param_list.index(name)+1])
        name = 'Kinetic Time (ms)'
        self.info[name] = float(param_list[param_list.index(name)+1])
        name = 'Shots per block'
        self.info[name] = int(param_list[param_list.index(name)+1])
        name = 'Total Shots'
        self.info[name] = int(param_list[param_list.index(name)+1])

    def load_data(self):
        if self.num == -1:
            x_name = f"{self.dir}{self.root_name}_X.bin"
            self.data['X_raw'] = load_binary_data(x_name)
            t_name = f"{self.dir}{self.root_name}_T.bin"
            self.data['T_raw'] = load_binary_data(t_name)
        
        else:
            x_name = f"{self.dir}{self.root_name}_X_{self.num}.bin"
            self.data['X_raw'] = load_binary_data(x_name)
            t_name = f"{self.dir}{self.root_name}_T{self.num}.bin"
            self.data['T_raw'] = load_binary_data(t_name)        

    
    def save_data(self, root_name):
        if self.num == -1:
            x_name = f"{self.dir}{root_name}_X.bin"
            save_binary_data(x_name, self.data['X_raw'])
            t_name = f"{self.dir}{root_name}_T.bin"
            save_binary_data(t_name, self.data['T_raw'])
        
        else:
            x_name = f"{self.dir}{root_name}_X_{self.num}.bin"
            save_binary_data(x_name, self.data['X_raw'])
            t_name = f"{self.dir}{root_name}_T{self.num}.bin"
            save_binary_data(t_name, self.data['T_raw'])   
    
    def set_time(self):
        self.data['T'] = self.data['T_raw'] * self.info['TOF Sweep (us)'] * 1e-6 
        self.data['X'] = self.data['X_raw'] * .1 # in ns, TOF time
    
    def cal_mass(self, alpha, beta):
        self.info['alpha']=alpha
        self.info['beta'] = beta
        #alpha:slope
        #beta: intercept
        # tof = m**.5 * alpha + beta
        self.data['M'] = ((self.data['X'] - beta)/alpha)**2

        self.set_bin_lim()


    def set_bin_lim(self):
        self.info['Mass range'] = ((self.info['TOF Sweep (us)']*1e3 - self.info['beta'])/self.info['alpha'])**2
   
    def find_sweep_inds(self):
        tof_end = np.argwhere(self.data['T_raw'][1:] < self.data['T_raw'][:-1])[:, 0]
        tof_begin = tof_end + 1
        tof_begin = np.insert(tof_begin[:-1], 0, 0.)
        self.data['TOF begin'] = tof_begin
        self.data['TOF end'] = tof_end

    def get_shot_range(self, first_shot: int, num_shots: int):
        num_shots -=1
        ind_0 = self.data['TOF begin'][first_shot]
        if first_shot + num_shots > len(self.data['TOF end']):
            ind_1 = self.data['TOF end'][-1]
        else:
            ind_1 = self.data['TOF end'][first_shot + num_shots]

        return ind_0, ind_1
    
    def bin_data(self, m_bin, t_bin):
        m_bins = np.arange(0., self.info['Mass range'], m_bin)
        actual_bin = np.ceil(t_bin / (self.info['TOF Sweep (us)'] * 1e-6)) * (self.info['TOF Sweep (us)'] * 1e-6)
        t_bins = np.arange(-15e-6, self.info['Kinetic Time (ms)']*1e-3, actual_bin)
        hist, tedge, medge = np.histogram2d(self.data['T'], self.data['M'], bins=(t_bins, m_bins))
        self.data['Binned'] = hist
        self.data['M edge'] = medge
        self.data['T edge'] = tedge

    def bin_raw_data(self, x_bin, t_bin):
        x_bins = np.arange(0., self.info['TOF Sweep (us)']*1000., x_bin )
        t_bins = np.arange(0., self.info['Kinetic Time (ms)']*1e-3, t_bin)
        hist, tedge, xedge = np.histogram2d(self.data['T'], self.data['X'], bins=(t_bins, x_bins))
        self.data['Binned raw'] = hist
        self.data['X edge'] = xedge
        self.data['T edge'] = tedge

    def bin_shot_range(self, m_bin, t_bin, first_shot, num_shots):
        i0, i1 = self.get_shot_range(first_shot, num_shots)
        m_bins = np.arange(0., self.info['Mass range'], m_bin)
        t_bins = np.arange(0., self.info['Kinetic Time (ms)']*1e-3, t_bin)
        hist, tedge, medge = np.histogram2d(self.data['T'][i0:i1], self.data['M'][i0:i1], bins=(t_bins, m_bins))
        return hist, tedge, medge

    def set_1D_axes(self):
        #Use the middle value of each bin for the mass values
        m_array = np.vstack([self.data['M edge'][:-1], self.data['M edge'][1:]])
        self.data['Masses'] = np.mean(m_array, axis=0)
        self.data['Mass counts'] = np.trapz(self.data['Binned'], axis=0)

        t_array = np.vstack([self.data['T edge'][:-1], self.data['T edge'][1:]])
        self.data['time'] = np.mean(t_array, axis=0)


    def vert_slice(self, mass1, mass2):
        ind1 = value_ind(self.data['M edge'], mass1)
        ind2 = value_ind(self.data['M edge'], mass2)
        
        slice_data = self.data['Binned'][:, ind1:ind2]
        v_slice = np.trapz(slice_data, axis=1)
        return v_slice
    
    def vert_slice_shot_range(self, m_bin, t_bin, mass1, mass2, first_shot, num_shots):
        hist, tedge, medge = self.bin_shot_range(m_bin, t_bin, first_shot, num_shots)

        ind1 = value_ind(medge, mass1)
        ind2 = value_ind(medge, mass2)
        
        slice_data = hist[:, ind1:ind2]
        v_slice = np.trapz(slice_data, axis=1)
        time = edge_to_mid(tedge)

        return time, v_slice
    
    def stack_vert_shot_range(self, m_bin, t_bin, mass1, mass2, num_shots):
        t, slc_array = self.vert_slice_shot_range(m_bin, t_bin, mass1, mass2, 0, num_shots)

        n_slices = int(np.floor(len(self.data['TOF end'])/num_shots))


        for i in range(n_slices):
            if i == 0:
                continue
            if i * num_shots >= len(self.data['TOF end']):
                break
            t, slc = self.vert_slice_shot_range(m_bin, t_bin, mass1, mass2, i * num_shots, num_shots)
            slc_array = np.vstack([slc_array, slc])
        
        return t, slc_array


class PIE:
    def __init__(self, directory, root_name):
        self.dir = directory
        self.root_name = root_name
        SEs = {}
    
    def load_data(self):
        dirs = os.listdir(self.dir)
        
        for d in dir:
            if f"{self.root_name}_T" in d:
                root_len =len(self.root_name) + len("_T")
                num = int(d[root_len:-4])
                self.SEs[num] = Single_E(self.dir, self.root_name)
                self.SEs[num].read_params()
