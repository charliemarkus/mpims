import mpims.mpims.data_handler as dh
import numpy as np
import lmfit

masses = np.array([28.031300,42.046950,56.0626])

def gauss(x, x0, a, w):
    return a * np.exp(-(x-x0)**2/(w/2.355)**2)

def gauss_p(Params: lmfit.Parameters, x):
    x0 = Params['x0'].value
    a = Params['a'].value
    w = Params['w'].value

    return gauss(x, x0, a, w)
def gauss_res(Params, x, y_exp):
    return y_exp - gauss_p(Params, x)

